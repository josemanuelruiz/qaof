﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Web;

namespace QAOF.Models
{
    public class Station : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private ObservableCollection<MachineStation> _machineStations;
        private ObservableCollection<StationOperationOrder> _stationOperationOrder;
        private ObservableCollection<OperationItem> _operationItems;

        public string CPLANT { get; set; }
        public string IMASTER { get; set; }
        public int TIPOBAUTIZO { get; set; }
        public Nullable<int> NEST_ENVIO_PLC { get; set; }
        public Nullable<int> NEST_TRAMO { get; set; }
        public Brush DESTADO_FORECOLOR { get; set; }
        public string CPDL_INI { get; set; }
        public Nullable<double> OFFSET_INI { get; set; }
        public string CPDL_FIN { get; set; }
        public Nullable<int> NPASOS { get; set; }
        public Nullable<double> OFFSET_Y { get; set; }
        public string CTRAMO_ANT { get; set; }
        public string CMODULO { get; set; }
        public string CMICRO { get; set; }
        public string CENCODER { get; set; }
        public string CLECTOR { get; set; }
        public string CPARO { get; set; }
        public string CPC { get; set; }
        public string CFOTOCEL { get; set; }
        public double NMETROSPASO { get; set; }
        public string CPTO_CHECKMAN { get; set; }
        public Nullable<int> NACTION_FILTER { get; set; }
        public string CEXPR_FILTER { get; set; }
        public string CEXPR_PARTS { get; set; }

        public string CLINEA { get; set; }
        public string CTRAMO { get; set; }
        public string DTRAMO { get; set; }
        public string CHABILIT { get; set; }
        public int NumPM { get; set; }
        public string DEST_TRAMO { get; set; }
        public string DEST_ENVIO_PLC { get; set; }
        public int NPLC_INDEX { get; set; }

        //public int GetPMCount(ObservableCollection<MachineStation> lstPuestosMaquina)
        //{
        //    int ret = 0;

        //    if (lstPuestosMaquina != null) ret = lstPuestosMaquina.Count(x => x.CPLANT.Equals(CPLANT) && x.CLINEA.Equals(CLINEA) && x.CTRAMO.Equals(CTRAMO));

        //    return ret;
        //}

        public ObservableCollection<MachineStation> MachineStations
        {
            get
            {
                return _machineStations;
            }
            set
            {
                _machineStations = value;
            }
        }

        public ObservableCollection<StationOperationOrder> StationOperationOrder
        {
            get
            {
                return _stationOperationOrder;
            }
            set
            {
                _stationOperationOrder = value;
            }
        }

        public ObservableCollection<OperationItem> OperationItems
        {
            get
            {
                return _operationItems;
            }
            set
            {
                _operationItems = value;
            }
        }

        public Operation CurrentOperation
        {
            get
            {
                return _stationOperationOrder.FirstOrDefault()?.Operation;
            }
        }

        public MachineStation CurrentMachineStation {
            get
            {
                return _machineStations.FirstOrDefault();
            }
        }

        int _currentNassOrder;
        public int CurrentNassOrder {
            get
            {
                // TODO - Eliminar cuando tengamos un código introducido por el usuario                
                return 150380;
            }
            set
            {
                _currentNassOrder = value;

            }
        }

        string _currentVehicle;
        public string CurrentVehicle
        {
            get
            {
                // TODO - Eliminar cuando tengamos un código introducido por el usuario                
                return "D50025";
            }
            set
            {
                _currentVehicle = value;

            }
        }

        string _currentUserName;
        public string CurrentUserName
        {
            get
            {
                // TODO - Eliminar cuando tengamos el nombre del usuairo logado
                return "Elena Nito";
            }
            set
            {
                _currentUserName = value;
            }
        }
    }
}