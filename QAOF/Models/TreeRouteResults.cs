﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QAOF.Models
{
    public class TreeRouteResults
    {
        public List<RouteResult> AddRange(List<RouteResult> routeResults)
        {
            List<RouteResult> treeRoutes = new List<RouteResult>();

            int parentId = 0;
            int i = 0;
            int routeResultId = 1;
            while (i < routeResults.Count - 1)
            {
                if (!treeRoutes.Any(to => to.COPERAC.Equals(routeResults[i].COPERAC)))
                {
                    parentId = 0;
                    var routes = routeResults.Where(r => r.COPERAC.Equals(routeResults[i].COPERAC)).OrderBy(o => o.NITEM).ToList();
                    if (routeResults.Any())
                    {
                        treeRoutes.Add(NewTreeOperation(new RouteResult { COPERAC = routeResults[i].COPERAC, DOPERAC = routeResults[i].DOPERAC }, parentId, routeResultId));
                        parentId = routeResultId++;
                        routes.ForEach(r =>
                        {
                            r.DOPERAC = "";
                            treeRoutes.Add(NewTreeOperation(r, parentId, routeResultId));
                            routeResultId++;
                            i++;
                        });
                    }
                }
                i++;
            }

            return treeRoutes;
        }

        private RouteResult NewTreeOperation(RouteResult route, int parentId, int id)
        {
            RouteResult routeResult = new RouteResult
            {
                CSECTION = route.CSECTION,
                COPERAC = route.COPERAC,
                NVERSION = route.NVERSION,
                DOPERAC = route.DOPERAC,
                NITEM = route.NITEM,
                DITEM = route.DITEM,
                CRESULT = route.CRESULT,
                DRESULT = route.DRESULT,
                RouteResultId = id,
                ParentId = parentId
            };

            return routeResult;
        }
    }
}