﻿using erQAIATools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QAOF.Models
{
    public class OperationItem
    {
        public string CPLANT { get; set; }
        public string CSECTION { get; set; }
        public string COPERAC { get; set; }
        public short NVERSION { get; set; }
        public decimal NITEM { get; set; }
        public string DITEM { get; set; }
        public string CPUESTO_MAQ { get; set; }
        public string CLINEA { get; set; }
        public string CTRAMO { get; set; }
        public int NORDERITEM { get; set; }
        public Nullable<int> CRULE { get; set; }
        public Nullable<int> NJOB { get; set; }
        public decimal NPROGR_NORM { get; set; }
        public decimal NPROGR_RECUP { get; set; }
        public decimal NPROGR_CHECK { get; set; }
        public Nullable<double> QPAR_MIN { get; set; }
        public Nullable<double> QPAR_MAX { get; set; }
        public Nullable<double> QPAR { get; set; }
        public Nullable<double> QANGULO { get; set; }
        public bool EsMaestro { get; set; }
        public string DRULE { get; set; }
        public string CEXPR { get; set; }
        public int? PARO_OPERACION { get; set; }
        public string DPARO_OPERACION => TipoParoOperacion.GetDescription(PARO_OPERACION);
        public string EN_VIGOR { get; set; }
    }
}