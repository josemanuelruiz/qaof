﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Web;

namespace QAOF.Models
{
    public class Operation
    {
        public Operation()
        {
        }

        public string CPLANT { get; set; }
        public string CSECTION { get; set; }
        public string COPERAC { get; set; }
        public short NVERSION { get; set; }
        public string DOPERAC { get; set; }
        public int NUM_APRIETES { get; set; }
        public string CIMAGEN { get; set; }

        public Nullable<DateTime> TACT { get; set; }
        public Brush DESTADO_FORECOLOR { get; set; }

        public bool PMEnProduccion { get; set; }
        public string PMEnProduccionSN => PMEnProduccion ? "SI" : "NO";

        public bool EnVigor { get; set; }
        public string EnVigorSN => EnVigor ? "SI" : "NO";

        public bool EnProduccion => (EnVigor && PMEnProduccion);
        public string EnProduccionSN => (EnVigor && PMEnProduccion ? "SI" : "NO");
    }
}