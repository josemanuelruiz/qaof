﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QAOF.Models
{
    public class StationOperationOrder
    {
        public string CPLANT { get; set; }
        public string CLINEA { get; set; }
        public string CTRAMO { get; set; }
        public string CSECTION { get; set; }
        public string COPERAC { get; set; }
        public short NVERSION { get; set; }
        public string POSITION { get; set; }

        public Operation Operation { get; set; }
        public Station Station { get; set; }
    }
}