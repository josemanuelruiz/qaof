﻿using AutoMapper;
using erQAIABR;
using erQAIABR.DTO;
using erQAIAEntities;
using System.Drawing;

namespace QAOF.Models.Mappings
{
    public class Mapper
    {
        public IMapper mapper;
        private static Mapper instance = null;
        private static readonly object padlock = new object();

        private string YtoSIConverter(string YN) { return (YN.Equals("Y")) ? "SI" : "NO"; }
        //private string GetParentID(string coperac) { return coperac; }
        //private string GetOperationDescription(string coperac, string doperac) { return coperac + " - " + doperac; }
        //private string GetOperItemDescription(decimal nitem, string ditem) { return nitem.ToString() + " " + ditem; }
        
        Mapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                // OPERACIONES
                cfg.CreateMap<OPERACIONE, Operation>().ForMember(dest => dest.NUM_APRIETES, opt => opt.MapFrom(src => 0));

                // APRIETES
                cfg.CreateMap<OPER_ITEMS, OperationItem>().ForMember(dest => dest.EsMaestro, opt => opt.MapFrom(src => 0));

                //// RUTAS_DET
                //cfg.CreateMap<RUTAS_DET, RouteDetail>().ForMember(dest => dest.ID, opt => opt.MapFrom(src => src.NITEM))
                //                                       .ForMember(dest => dest.ParentID, opt => opt.MapFrom(src => GetParentID(src.COPERAC)))
                //                                       .ForMember(dest => dest.OperationDescription, opt => opt.MapFrom(src => GetOperationDescription(src.COPERAC, src.OPERACIONE.DOPERAC)))
                //                                       .ForMember(dest => dest.OperItemDescription, opt => opt.MapFrom(src => GetOperItemDescription(src.NITEM, src.NITEM.ToString())));

                // RUTAS_DET
                cfg.CreateMap<RUTAS_DET_DTO, RouteResult>().ForMember(dest => dest.RouteResultId, opt => opt.MapFrom(src => 0))
                                                       .ForMember(dest => dest.ParentId, opt => opt.MapFrom(src => 0))
                                                       .ForMember(dest => dest.DOPERAC, opt => opt.MapFrom(src => src.OPERACIONE.DOPERAC))
                                                       .ForMember(dest => dest.DITEM, opt => opt.MapFrom(src => src.DITEM));

                cfg.CreateMap<PUESTOS_MAQUINA, MachineStation>().ForMember(dest => dest.CanBeEnabled, opt => opt.MapFrom(src => true));

                cfg.CreateMap<STATIONS_OPERATIONS_ORDER, StationOperationOrder>().ForMember(dest => dest.POSITION, opt => opt.MapFrom(src => 0));

                // ESTACIONES
                cfg.CreateMap<TRAMO, Station>().ForMember(dest => dest.NumPM, opt => opt.MapFrom(src => 0))  //erPuestoMaquina.GetByTramo(src.CPLANT, src.CLINEA, src.CTRAMO, null).Count())) OP OK
                                                .ForMember(dest => dest.CHABILIT, opt => opt.MapFrom(src => YtoSIConverter(src.CHABILIT)))
                                                .ForMember(dest => dest.DEST_TRAMO, opt => opt.MapFrom(src => (src.NEST_TRAMO.HasValue) ? erTramo.GetStateDescription((erTramo.Estados_Tramo)src.NEST_TRAMO) : ""))
                                                .ForMember(dest => dest.DESTADO_FORECOLOR, opt => opt.MapFrom(src => (src.NEST_TRAMO.HasValue) ? GlobalMethods.ConvertColorToBrush(GlobalMethods.ConvertDrawingToMediaColor(erTramo.GetBackColor((erTramo.Estados_Tramo)src.NEST_TRAMO))) : Brushes.Black))
                                                .ForMember(dest => dest.MachineStations, opt => opt.MapFrom(src => src.PUESTOS_MAQUINA))
                                                .ForMember(dest => dest.StationOperationOrder, opt => opt.MapFrom(src => src.STATIONS_OPERATIONS_ORDER))
                                                .ForMember(dest => dest.OperationItems, opt => opt.MapFrom(src => src.OPER_ITEMS));

                // PRODUCCIONES
                cfg.CreateMap<PRODUCCIONE, Production>().ForMember(dest => dest.Operation, opt => opt.MapFrom(src => src.OPERACIONE));
            });
            mapper = config.CreateMapper();
        }

        public static Mapper Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new Mapper();
                    }
                    return instance;
                }
            }
        }
    }
}