﻿using erQAIAEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QAOF.Models
{
    public class Production
    {
        public int OperationId { get; set; }
        public int ProductionId { get; set; }

        public int NPRODUCTION { get; set; }
        public Nullable<System.DateTime> TPRODUCTION { get; set; }
        public int NASS_ORDER { get; set; }
        public short NCONT_LANZAM { get; set; }
        public string CLANZAMIENTO { get; set; }
        public Nullable<System.DateTime> TLANZAMIENTO { get; set; }
        public short NCONT_SEC_MONTAJE { get; set; }
        public string CSEC_MONTAJE { get; set; }
        public string CVIN { get; set; }
        public string CMODULO { get; set; }
        public string CPLANT { get; set; }
        public string CLINEA { get; set; }
        public string CTRAMO { get; set; }
        public string CPUESTO_MAQ { get; set; }
        public Nullable<int> NPASO { get; set; }
        public string CSECTION { get; set; }
        public string COPERAC { get; set; }
        public short NVERSION { get; set; }
        public string DOPERAC { get; set; }
        public decimal NITEM { get; set; }
        public string CAPRIETE { get; set; }
        public Nullable<double> QPAR_OBJ { get; set; }
        public Nullable<double> QANGULO_OBJ { get; set; }
        public string CMAQUINA { get; set; }
        public Nullable<decimal> NPROGR { get; set; }
        public Nullable<int> NEST_MAQUINA { get; set; }
        public string CRESULT { get; set; }
        public string DRESULT { get; set; }
        public Nullable<double> QPAR { get; set; }
        public Nullable<double> QANGULO { get; set; }
        public string DCOMMENT { get; set; }
        public string CUSUARIO { get; set; }
        public string DTRAZA_PLC { get; set; }
        public string CPRODUCTO { get; set; }
        public Nullable<double> QRESULT_3 { get; set; }
        public string CTURNO { get; set; }
        public Nullable<decimal> NJOB { get; set; }
        public string TOOL_SERIAL_NUMBER { get; set; }

        public OperationItem OperationItem { get; set; }
        public Operation Operation { get; set; }
        public PLANTA PLANTA { get; set; }
        public ASSEMBLY_ORDERS ASSEMBLY_ORDERS { get; set; }
    }
}