﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QAOF.Models
{
    public class RouteResult
    {
        public int RouteResultId { get; set; }
        public int ParentId { get; set; }
        public string DOPERAC { get; set; }
        public string DITEM { get; set; }

        public int NASS_ORDER { get; set; }
        public string CPLANT { get; set; }
        public string CSECTION { get; set; }
        public Nullable<int> NRESTAPR { get; set; }
        public Nullable<int> NRESTPM { get; set; }
        public Nullable<int> NORDEN { get; set; }
        public string COPERAC { get; set; }
        public short NVERSION { get; set; }
        public decimal NITEM { get; set; }
        public string CRESULT { get; set; }
        public string DRESULT { get; set; }
        public Nullable<int> NPLC_INDEX { get; set; }
        public Nullable<int> NPRODUCTION { get; set; }
        public string CPLC { get; set; }
        public string CMODULO { get; set; }
        public Nullable<int> NSERVER_INDEX { get; set; }

        public Operation Operation { get; set; }
        //public OperationItem OperationItem { get; set; }
    }
}