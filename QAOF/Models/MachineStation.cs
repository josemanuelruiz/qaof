﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Web;

namespace QAOF.Models
{
    public class MachineStation
    {
        public MachineStation()
        {
            Operations = new ObservableCollection<Operation>();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public string CPLANT { get; set; }
        public string CLINEA { get; set; }
        public string CTRAMO { get; set; }
        public string CPUESTO_MAQ { get; set; }
        public string DPUESTO_MAQ { get; set; }
        public string CIP { get; set; }
        public int NPUERTO { get; set; }
        public Nullable<int> NPLC_INDEX { get; set; }
        public string DESTADO { get; set; }
        public Brush DESTADO_FORECOLOR { get; set; }
        public string CHABILIT { get; set; }
        public string CMAQUINA { get; set; }
        public Nullable<int> NESTADO { get; set; }
        public Nullable<DateTime> TACT { get; set; }
        public Nullable<DateTime> TFIN { get; set; }
        public bool EnVigor { get; set; }
        public bool EnProduccion { get; set; }
        public string EnVigorSN => EnVigor ? "SI" : "NO";
        public bool Habilitada => (CHABILIT == "SI" || CHABILIT == "Y");
        public bool CanBeEnabled => !Habilitada && (!string.IsNullOrEmpty(CMAQUINA));

        public ObservableCollection<Operation> Operations { get; set; }
    }
}