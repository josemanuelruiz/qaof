﻿using DevExpress.Web.Mvc;
using erQAIABR;
using erQAIABR.DTO;
using erQAIADataAccess;
using erQAIAEntities;
using Logs;
using QAOF.Models;
using QAOF.Models.Mappings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QAOF.Controllers
{
    public class StationController : Controller
    {
        // GET: Station
        public ActionResult Index()
        {
            return View();
        }

        // GET: Station/Details/5
        public ActionResult Station(string station)
        {
            List<Station> stations = GetStations();
            if (stations.Any())
                return View(stations.FirstOrDefault());
            else
                return View();
        }

        // GET: Stations
        public ActionResult Stations()
        {

            List<Station> stations = GetStations();

            return View(stations);
        }

        protected List<Station> GetStations()
        {
            LoginInfo.Line = "2";
            List<TRAMO> tramos = erTramo.GetActiveStations(true);
            List<Station> stations = Mapper.Instance.mapper.Map<List<TRAMO>, List<Station>>(tramos);

            // TODO - Operation no se está mapeando
            OPERACIONE OPERACION = new OPERACIONE
            {
                CPLANT = "PLANTA",
                COPERAC = "OPERACION_1",
                DOPERAC = "DESCRIPCIÓN OPERACION 1"
            };
            stations.ForEach(s =>
            {
                foreach (var operorder in s.StationOperationOrder)
                    operorder.Operation = Mapper.Instance.mapper.Map<OPERACIONE, Operation>(OPERACION);
            });

            Session["station"] = stations.FirstOrDefault();
            HttpCookie cookie = new HttpCookie("stationName", stations.FirstOrDefault().DTRAMO);
            HttpContext.Response.SetCookie(cookie);

            return stations;
        }

        [ValidateInput(false)]
        public ActionResult GridViewProductionPartial(string lineCode = "L200", string stationCode = "L231_V2", int nassorder = 150380)
        {
            string where = $"PR.CLINEA = '{lineCode}' AND PR.CTRAMO = '{stationCode}'";
            List<int> nassorderList = new List<int> { nassorder };
            List<Production> productions = GlobalMethods.ConvertTo<Production>(Factory.GetProduccionesWhere(nassorderList, WhereAdicional: where));

            productions =  productions.OrderBy(p => p.COPERAC).ToList();

            productions.ForEach(p => System.Diagnostics.Debug.WriteLine(p.OperationId + " - " + p.NPRODUCTION));

            var model = productions;
            return PartialView("_GridViewProductionPartial", model);
        }

        [ValidateInput(false)]
        public ActionResult TreeRouteResultPartial(string plantCode = "ZF_MONTAJE", string lineCode = "L200", string stationCode = "L231_V2", int nassorder = 150380)
        {
            LoginInfo.Line = "2";
            List<RouteResult> routeDetails = GetRouteResults(plantCode, lineCode, stationCode, nassorder);
            var model = routeDetails;

            //model.ForEach(p => System.Diagnostics.Debug.WriteLine(p.ParentId + " - " + p.RouteResultId));
            return PartialView("_TreeRouteResultPartial", model);
        }

        /// <summary>
        /// Obtiene las rutas detalle correspondientes a una estación y un vehículo
        /// </summary>
        /// <param name="plantaCode">Código de planta</param>
        /// <param name="stationCode">Código de estación</param>
        /// <returns>Operaciones y sus aprietes</returns>
        protected List<RouteResult> GetRouteResults(string plantCode, string lineCode, string stationCode, int nassorder)
        {
            LoginInfo.Line = "2";
            List<RUTAS_DET_DTO> lstOPERACIONES = erRutasDet.GetByStation(plantCode, lineCode, stationCode, nassorder, true);
            List<RouteResult> routeResult = Mapper.Instance.mapper.Map<List<RUTAS_DET_DTO>, List<RouteResult>>(lstOPERACIONES);

            TreeRouteResults treeRouteResults = new TreeRouteResults();

            List<RouteResult> model = treeRouteResults.AddRange(routeResult);

            return model;
        }

        [ValidateInput(false)]
        public ActionResult CardViewOperationPartial()
        {
            List<Station> model = new List<Station>();
            var station = Session["station"] as Station;

            HttpCookie cookie = null;
            if (HttpContext.Request.Cookies["stationName"] != null)
            {
                cookie = HttpContext.Request.Cookies.Get("stationName");
                HttpContext.Response.Cookies.Remove("stationName");
                HttpContext.Response.SetCookie(cookie);
                string stationName = cookie.Value;
            }

            if (station != null)
                model.Add(station);

            return PartialView("_CardViewOperationPartial", model);
        }
    }
}
