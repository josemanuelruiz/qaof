﻿using Logs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace QAOF.SQLSubscription
{
    public class hlpSQLSubscriptions : IDisposable
    {
        #region VARIABLES GLOBALES

        private bool disposed;
        private SqlConnection _connection = null;

        private SqlDependency depRutasDet = null;
        private SqlCommand cmdRutasDet = null;
        private DataSet dsToWatchRutasDet = null;
        //public List<DEF.spGetLayoutData> lstRutasDet = null;
        public int changeCountRutasDet = 0;
        private SqlNotificationEventArgs LastSqlNotificationRutasDet = null;
        public delegate bool RutasDet_DataChanged();
        public static event RutasDet_DataChanged OnRutasDetDataChanged;

        #endregion

        #region PROPERTIES
        private static string ClassName
        {
            get
            {
                return System.Reflection.MethodBase.GetCurrentMethod().DeclaringType?.Name;
            }
        }

        private static string providerConnectionString
        {
            get
            {

                return ConfigurationManager.ConnectionStrings["QuaiaEntityModelL1"].ConnectionString;
                    //[ConfigurationManager.AppSettings["UseConnectionLinea1"]].ConnectionString;
            }
        }
        #endregion

        #region CONSTRUCTOR
        public hlpSQLSubscriptions()
        {
            // Remove any existing dependency connection, then create a new one.
            Subscription();

            // Define conexion
            _connection = new SqlConnection(providerConnectionString);
        }

        private void Subscription()
        {
            // Remove any existing dependency connection, then create a new one.
            SqlDependency.Stop(providerConnectionString);
            SqlDependency.Start(providerConnectionString);
        }
        #endregion

        #region DISPOSE
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    // Remove any existing dependency connection, then create a new one.                    
                    if (depRutasDet != null) depRutasDet.OnChange -= dependency_OnChangeRutasDet;

                    SqlDependency.Stop(providerConnectionString);

                    cmdRutasDet?.Dispose();
                    dsToWatchRutasDet?.Dispose();

                    if (_connection != null)
                    {
                        if (_connection.State == ConnectionState.Open) _connection.Close();
                        _connection.Dispose();
                    }
                }

                // Dispose unmanaged resources here.
            }

            disposed = true;
        }
        #endregion

        #region PUESTOS_MAQUINA SUBSCRIPTION
        public void SQLSubscribeToRutasDet()
        {
            try
            {
                // Crea la consulta                            
                cmdRutasDet = new SqlCommand(GetSQLPM(), _connection);

                // Crea el dataset
                dsToWatchRutasDet = new DataSet();
                //lstRutasDet = new List<DEF.spGetLayoutData>();

                // Lee rutas detalle
                GetDataRutasDet();
            }
            catch (Exception ex)
            {
                sLogger.WriteError(ex);
            }
        }

        private string GetSQLPM()
        {
            string sSQL = "";

            try
            {
                sSQL = string.Format("SELECT CPLANT, CLINEA, CTRAMO, CPUESTO_MAQ, CMAQUINA, TACT, TFIN,"
                    + " NEST_ENVIO_PLC FROM DBO.PUESTOS_MAQUINA ");

                sLogger.Write(LogTypeEnum.Debug, "GetSQLPM", sSQL);
            }
            catch (Exception ex)
            {
                sLogger.WriteError(ex);
            }

            return sSQL;
        }

        private void GetDataRutasDet()
        {
            string FuncName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                // Make sure the command object does not already have
                // a notification object associated with it.
                cmdRutasDet.Notification = null;

                // Create and bind the SqlDependency object
                // to the command object.
                depRutasDet = new SqlDependency(cmdRutasDet);
                depRutasDet.OnChange += dependency_OnChangeRutasDet;

                lock (dsToWatchRutasDet)
                {
                    // Empty the dataset so that there is only
                    // one batch of data displayed.
                    dsToWatchRutasDet.Clear();

                    using (SqlDataAdapter adapter = new SqlDataAdapter(cmdRutasDet))
                    {
                        adapter.Fill(dsToWatchRutasDet, "RutasDet");
                    }
                }

                sLogger.Write(LogTypeEnum.Debug, FuncName, $"Añadido evento. RutasDet actualizada {changeCountRutasDet} veces. Registros: {dsToWatchRutasDet.Tables[0].Rows.Count}");

                // Evento data changed
                OnRutasDetDataChanged?.Invoke();
            }
            catch (Exception ex)
            {
                sLogger.WriteError(ex);
            }
        }

        private void dependency_OnChangeRutasDet(object sender, SqlNotificationEventArgs e)
        {
            string FuncName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                if (e.Info == SqlNotificationInfo.Invalid || e.Info == SqlNotificationInfo.Query) return;

                ++changeCountRutasDet;
                sLogger.Write(LogTypeEnum.Debug, FuncName, string.Format("Quitado evento. RutasDet actualizada {0} veces.", changeCountRutasDet));

                // Remove the handler, since it is only good for a single notification.
                SqlDependency dependency = (SqlDependency)sender;
                dependency.OnChange -= dependency_OnChangeRutasDet;

                LastSqlNotificationRutasDet = e;

                // Reload the dataset that is bound to the grid.
                GetDataRutasDet();
            }
            catch (Exception ex)
            {
                sLogger.WriteError(ex);
            }
        }

        public DataSet GetPM()
        {
            DataSet ret = new DataSet();

            try
            {
                // Crea la consulta            
                SqlConnection connection = new SqlConnection(providerConnectionString);
                SqlCommand cmd = new SqlCommand(GetSQLPM(), connection);
                using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                {
                    adapter.Fill(ret, "RutasDet");
                }
                cmd.Dispose();
                connection.Close();
                connection.Dispose();
            }
            catch (Exception ex)
            {
                sLogger.WriteError(ex);
            }

            return ret;
        }
        #endregion

    }
}